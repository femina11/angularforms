import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { FormServiceService } from '../../form-service.service';
import { IForm } from 'src/app/form';

@Component({
  selector: 'app-form',
  templateUrl: './app-form.component.html',
  styleUrls: ['./app-form.component.scss']
})
export class AppFormComponent implements OnInit {

  public response: object;

  registrationForm: FormGroup;

  public formInfo: IForm;

  reviewForm() {
    this.formServiceService.sendData(this.registrationForm.value);
    this.router.navigate(['review']);
  }

  constructor(private route: ActivatedRoute,
              private router: Router,
              private formServiceService: FormServiceService,
              private fb: FormBuilder) { }

  ngOnInit() {

    this.registrationForm = this.fb.group({
        firstName: ['', [Validators.required, Validators.minLength(3)]],
        lastName: ['', [Validators.required]]
    });

    this.formInfo = this.formServiceService.getData();

    this.route.paramMap.subscribe((params: ParamMap) => {
      const paramValue = params.get('new');

      if (!paramValue && this.formInfo) {
        this.registrationForm.setValue({
          firstName: this.formInfo.firstName,
          lastName: this.formInfo.lastName
      });
    }
    });
  }
}
