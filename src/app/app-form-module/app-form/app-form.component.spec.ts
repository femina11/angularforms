import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppFormComponent } from './app-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatDividerModule } from '@angular/material/divider';
import { MatButtonModule } from '@angular/material/button';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule } from '@angular/material/dialog';
import { HttpClientModule } from '@angular/common/http';
import { MatToolbarModule } from '@angular/material/toolbar';
import { RouterModule } from '@angular/router';

describe('AppFormComponent', () => {
  let component: AppFormComponent;
  let fixture: ComponentFixture<AppFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppFormComponent ],
      imports: [ReactiveFormsModule,
        MatButtonModule, MatFormFieldModule, MatInputModule, MatDividerModule,
        MatInputModule, MatToolbarModule, MatDialogModule,
        RouterModule.forRoot([]), HttpClientModule, BrowserAnimationsModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('First Name - Should be valid', () => {
    let firstName = component.registrationForm.controls['firstName'];
    expect(firstName.valid).toBeFalsy();
    expect(firstName.pristine).toBeTruthy();
    expect(firstName.errors['required']).toBeTruthy();
    firstName.setValue('abc');

    expect(firstName.errors).toBeNull();
  });

  it('First Name - Should be entered', () => {
    let firstName = component.registrationForm.controls['firstName'];
    expect(firstName.valid).toBeFalsy();
    expect(firstName.pristine).toBeTruthy();
    expect(firstName.errors['required']).toBeTruthy();
    firstName.setValue('');

    expect(firstName.invalid).toBeTruthy();
    expect(firstName.errors['required']).toBeTruthy();
  });

  it('First Name - Should be minimum of 3 characters', () => {
    let firstName = component.registrationForm.controls['firstName'];
    expect(firstName.valid).toBeFalsy();
    expect(firstName.pristine).toBeTruthy();
    expect(firstName.errors['required']).toBeTruthy();
    firstName.setValue('ab');

    expect(firstName.invalid).toBeTruthy();
    expect(firstName.errors['minlength']).toBeTruthy();
  });

  it('Last Name - Should be valid', () => {
    let lastName = component.registrationForm.controls['lastName'];
    expect(lastName.valid).toBeFalsy();
    expect(lastName.pristine).toBeTruthy();
    expect(lastName.errors['required']).toBeTruthy();
    lastName.setValue('abc');

    expect(lastName.valid).toBeTruthy();
    expect(lastName.errors).toBeNull();
  });

  it('Last Name - Should be entered', () => {
    let lastName = component.registrationForm.controls['lastName'];
    expect(lastName.valid).toBeFalsy();
    expect(lastName.pristine).toBeTruthy();
    expect(lastName.errors['required']).toBeTruthy();
    lastName.setValue('');

    expect(lastName.invalid).toBeTruthy();
    expect(lastName.errors['required']).toBeTruthy();
  });
});
