import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppReviewComponent } from './app-review/app-review.component';

const routes: Routes = [
  {path: '', component: AppReviewComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppReviewRoutingModule { }
