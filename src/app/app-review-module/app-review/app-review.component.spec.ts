import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppReviewComponent } from './app-review.component';
import { ReactiveFormsModule } from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatDividerModule} from '@angular/material/divider';
import {MatListModule} from '@angular/material/list';
import {MatToolbarModule} from '@angular/material/toolbar';
import { HttpClientModule } from '@angular/common/http';
import {MatDialogModule} from '@angular/material/dialog';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('AppReviewComponent', () => {
  let component: AppReviewComponent;
  let fixture: ComponentFixture<AppReviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppReviewComponent ],
      imports: [ReactiveFormsModule,
        MatButtonModule, MatFormFieldModule, MatInputModule, MatDividerModule,
        MatListModule, MatToolbarModule, MatDialogModule,
        RouterModule.forRoot([]), HttpClientModule, BrowserAnimationsModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppReviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
