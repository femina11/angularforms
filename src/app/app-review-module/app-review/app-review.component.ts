import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { FormServiceService } from '../../form-service.service';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'success-dialog',
  templateUrl: 'successDia.html',
})
export class successDialog { }

@Component({
  selector: 'app-review',
  templateUrl: './app-review.component.html',
  styleUrls: ['./app-review.component.scss']
})
export class AppReviewComponent implements OnInit {

  public firstName: string;

  registrationForm: FormGroup;


  public formInfo;

  constructor(private activatedRoute: ActivatedRoute,
              private router: Router,
              private formServiceService: FormServiceService,
              private fb: FormBuilder,
              public dialog: MatDialog) { }

  reviewForm() {
    this.formServiceService.sendData(this.registrationForm.value);
    this.router.navigate(['form']);
  }

  goSubmit() {
    this.formServiceService.submitData(this.registrationForm.value)
      .subscribe(data => {
        console.log(data);
        this.dialog.open(successDialog);
        this.router.navigate(['form', { new: true }]);
      }
      );
  }

  ngOnInit() {

    this.registrationForm = this.fb.group({
      firstName: [''],
      lastName: ['']
    });

    this.formInfo = this.formServiceService.getData();
    if (this.formInfo) {
      this.registrationForm.setValue({
        firstName: this.formInfo.firstName,
        lastName: this.formInfo.lastName
      });
    }
  }

}
