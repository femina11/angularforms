import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppReviewComponent, successDialog } from './app-review/app-review.component';
import { AppReviewRoutingModule } from './app-review-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatDividerModule} from '@angular/material/divider';
import {MatListModule} from '@angular/material/list';
import {MatToolbarModule} from '@angular/material/toolbar';
import { HttpClientModule } from '@angular/common/http';
import {MatDialogModule} from '@angular/material/dialog';


@NgModule({
  declarations: [AppReviewComponent,
    successDialog],
  imports: [
    CommonModule,
    AppReviewRoutingModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatDividerModule,
    MatListModule,
    MatToolbarModule,
    HttpClientModule,
    MatDialogModule

  ],
  exports: [
    AppReviewComponent
  ],
  entryComponents: [successDialog]
})
export class AppReviewModule { }
