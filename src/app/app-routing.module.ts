import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppFormModule } from './app-form-module/app-form-module.module';
import { AppFormComponent } from './app-form-module/app-form/app-form.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

const routes: Routes = [
  {path: '', redirectTo: '/form', pathMatch: 'full'},
  {path: 'form', loadChildren: () =>
  import('./app-form-module/app-form-module.module').then(m => m.AppFormModule)},
  {path: 'review', loadChildren: () =>
  import('./app-review-module/app-review-module.module').then(m => m.AppReviewModule)},
  {path: '**', component: PageNotFoundComponent}
 ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
