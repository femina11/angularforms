import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IForm } from '../app/form';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FormServiceService {

  public formData: object;
  private formInfo = new Subject<IForm>();

  formInfoData$ = this.formInfo.asObservable();

  private url = 'https://reqres.in/api/users/';
  fullData: IForm;
  constructor(private http: HttpClient) { }

  sendData(formData: IForm) {
    this.fullData = formData;
  }

  getData(): IForm {
    return this.fullData;
  }

  submitData(formData): Observable<IForm> {
    return this.http.post<IForm>(this.url, formData);
  }
}
